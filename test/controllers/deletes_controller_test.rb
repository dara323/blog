require 'test_helper'

class DeletesControllerTest < ActionController::TestCase
  setup do
    @delete = deletes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:deletes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create delete" do
    assert_difference('Delete.count') do
      post :create, delete: { body: @delete.body, title: @delete.title }
    end

    assert_redirected_to delete_path(assigns(:delete))
  end

  test "should show delete" do
    get :show, id: @delete
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @delete
    assert_response :success
  end

  test "should update delete" do
    patch :update, id: @delete, delete: { body: @delete.body, title: @delete.title }
    assert_redirected_to delete_path(assigns(:delete))
  end

  test "should destroy delete" do
    assert_difference('Delete.count', -1) do
      delete :destroy, id: @delete
    end

    assert_redirected_to deletes_path
  end
end
