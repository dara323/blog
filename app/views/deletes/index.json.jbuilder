json.array!(@deletes) do |delete|
  json.extract! delete, :id, :title, :body
  json.url delete_url(delete, format: :json)
end
