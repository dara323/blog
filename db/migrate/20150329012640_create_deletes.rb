class CreateDeletes < ActiveRecord::Migration
  def change
    create_table :deletes do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
